const speechOutput = document.querySelector("#invoiceInput");

const nAgt = navigator.userAgent;
let synth;
let recognition;

// check if browser is Chrome
function isChromeBrowser() {

  // check presence of chrome and edge in userAgent
  if ((verOffset = nAgt.indexOf("Chrome")) != -1  && nAgt.indexOf("Edge") == -1) {
    return true;
  } else {
    return false;
  }
}

if (isChromeBrowser()) {
  window.SpeechRecognition =
    webkitSpeechRecognition || window.SpeechRecognition;
  synth = window.speechSynthesis;
  recognition = new SpeechRecognition();
  recognition.interinResult = true;
} else {
  $(".startVoice").hide();
}

const dictate = () => {
  recognition.start();
  recognition.onresult = event => {
    const speechTotext = Array.from(event.results)
      .map(result => result[0])
      .map(result => result.transcript)
      .join(" ")
      .replace(" ", "");
    speechOutput.value = speechTotext;
    showContent("invoiceContainer");
    speak(`Kindly confirm the invoice is ${speechTotext}`);
  };
  recognition.onerror = function(event) {
    showContent("voiceError");
  };
};

const speak = utteranceAction => {
  if (typeof utteranceAction === "string") {
    const utterThis = new SpeechSynthesisUtterance(utteranceAction);
    synth.speak(utterThis);
  } else {
    const utteranceResponse = utteranceAction();
    const utterThis = new SpeechSynthesisUtterance(utteranceResponse);
    synth.speak(utterThis);
  }
};

const showContent = function(id) {
  $(".content-container").hide();
  $(`#${id}`).show();
};

$(document).ready(function() {
  //enable voice speaker
  $(".startVoice").click(function() {
    speechOutput.value = "";
    dictate();
    showContent("voiceListenContainer");
  });
  //show invoice container when dismiss button click
  $("#dismissVoice").click(function() {
    recognition.stop();
    showContent("invoiceContainer");
  });

  // get Invoice details on click of proceed button
  $("#btnProceed").click(function() {
    let num = $("#invoiceInput").val();
    localStorage.setItem("Invoice Number", num.toString());
    window.location.href = "invoice.html";
  });
});
