'use strict';

let invoiceDetails = 0;
let invoiceDetailsObj;
let url = window.location.href;
let testUrl = "https://bdaservice.test.vggdev.com";
let prodUrl = "https://bdaservice.prod.vggdev.com";

let correctUrl;

if (url.match("localhost") || url.match("127.0.0.1") || url.match("test.vggdev")) {
    correctUrl = testUrl;
} else {
    correctUrl = prodUrl;
}

//This block of code might be needed latter
let percentage;
// function to calculate percentage of outstanding amount
function getPercentage(num, per) {
    percentage =((num / 100) * per).toFixed(2);
    return $(".percentage-amt").html(`&#8358;${percentage}`);
}

//function to populate table
function populateTable(tableDetails) {
    let trHTML = "";
    if (tableDetails.length === 0) {
        $(".payment-table-container").hide();
        $(".no-history").show();
    } else {

        tableDetails.map((item, i) => {
            let pageCount = i + 1;

            trHTML += '<tr>' +
                '<td>' + pageCount + '</td>' +
                '<td>' + item.SubinvoiceNumber + '</td>' +
                '<td>' + item.RRR + '</td>' +
                '<td>' + (item.IsPaid ? 'Paid' : 'Not Paid') + '</td>' +
                '<td>' + item.PaymentDate + '</td>' +
                '<td>' + numbersWithCommas(item.Amount) + '</td>' +
                '</tr>';

        });

        $("#invoice-table-body").append(trHTML);
    }
}

//Ajax call to get invoice details
function getInvoiceDetails(invoiceNo) {
    $("#loader").show();
    displayLoader("show", "No hiding", $(".loader"));
    disableEnableButton("#btn-proceed", true);

    // $("#invoice-input-modal").css({display: "block"});

    $.ajax({
        headers: {
            'Content-Type': 'application/json'
        },
        url: `${correctUrl}/api/Open/SearchInvoice?invNum=${invoiceNo}`,
        type: 'GET',
        dataType: 'json', // added data type
        success: function (res) {
            // debugger;
            displayLoader("no show", " hide", $(".loader"));
            disableEnableButton("#btn-proceed", false);


            invoiceDetails = res.invoice;

            invoiceDetailsObj = invoiceDetails; //set invoiceDetailsObj

            //fill invoice header with details
            $("#invoiceNo").html(`#${invoiceDetails.InvoiceNumber}`);
            $("#invoiceOwner").text(invoiceDetails.Name);
            $("#revenue-heading").text(invoiceDetails.RevenueName);
            $("#agency-heading").text(invoiceDetails.AgencyName);
            // $("#rrr-heading").text(invoiceDetails.RRR);
            $("#email").text(invoiceDetails.Email);
            $("#phoneNo").text(invoiceDetails.PhoneNumber);
            $("#address").text(invoiceDetails.Address);
            $("#invoiceAmt").html(`&#8358;  ${numbersWithCommas(invoiceDetails.Amount)}`);
            $("#amountPaid").html(`&#8358; ${numbersWithCommas(invoiceDetails.AmountPaid)}`);
            $("#outstanding").html(`&#8358;  ${numbersWithCommas(invoiceDetails.Outstanding)}`);
            $(".modal-invoice-no").html(`# ${invoiceDetails.InvoiceNumber}`);
            $(".modal-outstanding-pyt").html(`#${numbersWithCommas(invoiceDetails.Outstanding)}`);
            $("#minPercent").html(`${invoiceDetails.PercentagePartPayment}%`);
            $(".inv-amt").html(`${numbersWithCommas(invoiceDetails.MinimumPartPayment)}`);
            $(".max-amt").html(`${invoiceDetails.NumberOfPayment}`);


            invoiceDetails.RRR !== null ? $("#rrr-heading").html(invoiceDetails.RRR) : $("#rrr-heading").html(" ");

            //set remita form input values
            $("#merchantId").val(invoiceDetails.MerchantId);
            $("#hash").val(invoiceDetails.Hash);
            $("#rrr").val((invoiceDetails.RRR !== null) ? invoiceDetails.RRR : "");
            $("#responseUrl").val(invoiceDetails.ResponseUrl);

            //Check if RRR Heading is empty and hide the RRR text otherwise show it
            if (!$.trim($('#rrr-heading').html() ).length) {
                $("#rrr-text").hide();
            }
            else {
            $("#rrr-text").show();
            }


            if (invoiceDetails.Outstanding === 0) {
                $(".payment-button").hide();
            }

            // console.log('inputs', $("#merchantId").val(),  $("#hash").val(),  $("#rrr").val(), $("#responseUrl").val())

            //Set remita form action
            document.SubmitRemitaForm.action = invoiceDetails.BillerUrl;

            // //call getPercentage function
            getPercentage(invoiceDetails.Outstanding, invoiceDetails.PercentagePartPayment);

            let tableDetails = res.subInvoices; //variable for populating table
            populateTable(tableDetails);

            $("#loader").hide();

            //hide wrong invoice modal
            $("#errorModal").modal('hide');
            $('.modal-backdrop').remove()


        },
        error: function (error) {
            // console.log("error", error);
            $("#loader").hide();
            displayLoader("no show", " hide", $(".loader"));
            disableEnableButton("#btn-proceed", false);


            let errorMessage = "";

            if (error.responseText) {
                errorMessage += error.responseText.replace(/^"(.*)"$/, '$1');
                $("#errorModal").modal("show");
                $('#errorMsg').text(errorMessage)
            } else if (errorMessage.length > 0) { //handle service unavailable
                errorMessage += "Please check your internet connection";
                $("#warning-msg").text(errorMessage);
                $("#warning-modal").modal("show");
            }
        },
        statusCode: {
            503: function (responseObject, textStatus, errorThrown) {
                // console.log('responseObject', responseObject, 'textStatus', textStatus, 'errorThrown', errorThrown);
                let errorMessage = "Service not available. Please try again later.";
                $("#warning-msg").text(errorMessage);
                $("#warning-modal").modal("show");
            }
        }

    });

}


//Ajax call to get RRR for part payment
$("#part-pyt-btn").click(function () {

    displayLoader("show", "No hiding", $(".loader"));
    disableEnableButton("#part-pyt-btn", true);

    let partPytAmount = parseInt($("#part-payment-value").val());

    let payload = {
        "InvoiceNumber": invoiceDetailsObj.InvoiceNumber,
        "AgencyId": invoiceDetailsObj.AgencyId,
        "Amount": partPytAmount
    };
    
    $.ajax({
        type: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        data: JSON.stringify(payload),
        cache: true,
        url: `${correctUrl}/api/Open/GenerateSubInvoice`,
        dataType: "json",
        success: function (res) {
            displayLoader("no show", " hide", $(".loader"));
            disableEnableButton("#part-pyt-btn", false);
            $("#part-payment-modal").hide();

            let response = res;

            // //set remita form input values
            $("#merchantId").val(response.MerchantId);
            $("#hash").val(response.Hash);
            $("#rrr").val(response.RRR);
            $("#responseUrl").val(response.ResponseUrl);

            // //Set remita form action
            document.SubmitRemitaForm.action = invoiceDetails.BillerUrl;

            // //set details for see rrr modal
            $("#rrr_number").html(`RRR: ${response.RRR}`);
            $(".amount").text(numbersWithCommas(partPytAmount));


            $("#see-rrr-modal").modal('show');

        },
        error: function (error) {
            displayLoader("no show", " hide", $(".loader"));
            disableEnableButton("#part-pyt-btn", false);
            $("#part-payment-modal").hide();
            $('.modal-backdrop').remove();


            let errorMessage = "";
            if (error.responseJSON) {
                errorMessage += error.responseJSON.Message.replace(/^"(.*)"$/, '$1');
                $("#warning-msg").text(errorMessage);
                $("#warning-modal").modal('show');
            } else {
                errorMessage += "Service not available";
                $("#warning-msg").text(errorMessage);
                $("#warning-modal").modal('show');
            }
        },
        statusCode: {
            503: function (responseObject, textStatus, errorThrown) {
                    let errorMessage = "Service not available. Please try again later.";
                $("#warning-msg").text(errorMessage);
                $("#warning-modal").modal("show");
            }
        }
    })


});

$(document).ready(function () {
    // debugger;
    let invoiceNo = localStorage.getItem('Invoice Number');
    let rightInvoiceNo = localStorage.getItem('Right Invoice');
    let newInvoiceNo = localStorage.getItem('New Invoice');
    if (invoiceNo !== null) {
        getInvoiceDetails(invoiceNo);
    } else if (rightInvoiceNo !== null) {
        getInvoiceDetails(rightInvoiceNo);
    } else if (newInvoiceNo !== null) {
        getInvoiceDetails(newInvoiceNo);
    }

});

//Ajax call to handle wrong invoice number
$("#wrongInvoiceProceed").click(function () {
    let invoiceNo = $("#wrongInvoiceInput").val();
    localStorage.removeItem("Invoice Number");
    localStorage.setItem('Right Invoice', invoiceNo);
    getInvoiceDetails(invoiceNo);
});


//send full payment details
$("#full-payment").click(function () {
    let rrrInput = $("#rrr").val();

    if (rrrInput === "") {
        $("#warning-msg").text("Generating RRR...");
        $("#warning-modal").css("color", "#000000");
        $("#warning-modal").modal("show");
        generateRRR();
    } else {
        $("#submit-btn").trigger("click");
    }

});

// generate RRR for full payment
function generateRRR() {
    $.ajax({
        type: "GET",
        headers: {
            "Content-Type": "application/json"
        },
        cache: true,
        url: `${correctUrl}/api/open/GenerateRRR?invNum=${invoiceDetailsObj.InvoiceNumber}`,
        dataType: "json",
        success: function (res) {
            displayLoader("no show", " hide", $(".loader"));

            $("#warning-modal").modal('hide');
            $('.modal-backdrop').remove()

            let response = res;

            // //set remita form rrr input value
            if (response.RRR !== null && response.RRR !== "") {
                $("#merchantId").val(response.MerchantId);
                $("#hash").val(response.Hash);
                $("#rrr").val(response.RRR);
                $("#responseUrl").val(response.ResponseUrl);
               
                $("#submit-btn").trigger("click");
            } else {
                $("#warning-modal").css("color", "orangered");
                $("#warning-msg").text("RRR could not be generated for this invoice. Try again later.");
                $("#warning-modal").modal("show");
            }
        
        },
        error: function (error) {

            // console.log(error);
            let errorMessage = "";
            if (error.responseJSON) {
                errorMessage += error.responseJSON.Message.replace(/^"(.*)"$/, '$1');
                $("#warning-msg").text(errorMessage);
                // $("#see-rrr-Btn").trigger('click');
                // $("#see-rrr-modal").modal('show');
                $("#warning-modal").modal('show');
            } else {
                errorMessage += "Service not available";
                $("#warning-msg").text(errorMessage);
                $("#warning-modal").modal('show');
            }
        },
        statusCode: {
            503: function (responseObject, textStatus, errorThrown) {
                // console.log('responseObject', responseObject, 'textStatus', textStatus, 'errorThrown', errorThrown);
                let errorMessage = "Service not available. Please try again later.";
                $("#warning-msg").text(errorMessage);
                $("#warning-modal").modal("show");
            }
        }
    })
}

//search new invoice number on invoice page
$("#btn-proceed").click(function () {
    $("#invoice-table-body").empty();
    $("#invoice-input-modal").modal('hide');
    let newInvoiceNo = $("#invoiceInput").val();
    getInvoiceDetails(newInvoiceNo);
    localStorage.removeItem('Invoice Number');
    localStorage.removeItem('Right Invoice');
    localStorage.setItem('New Invoice', newInvoiceNo);
    $('.modal-backdrop').remove()
    resetInputs("#invoiceInput");
    $(".new-invoiceNo").text("");
});


//clear Input values
$(".btn-cancel").click(function () {
    resetInputs("#part-payment-value");
    resetInputs("#invoiceInput");
    $(".new-invoiceNo").text("");
    $('.modal-backdrop').remove()
});

//clear backdrop and reload page to get new RRR details on the tahle
$(".close-rrr-info").click(function () {
    $('.modal-backdrop').remove();
    location.reload();
});

$("#invoiceInput").on('input', (function () {
    let inputValue = $("#invoiceInput").val();
    $(".new-invoiceNo").text(inputValue);
}));

//redirect to remita when pay button has been clicked on part payment
$("#pay-btn").click(function () {
    //  console.log('inputs', $("#merchantId").val(),  $("#hash").val(),  $("#rrr").val(), $("#responseUrl").val())
    // console.log('action', document.SubmitRemitaForm.action);
    $("#submit-btn").trigger("click");
});

//function to reset Inputs
function resetInputs(identifier) {
    return $(identifier).val("");
}


//function to hide or show loader
function displayLoader(show, hide, identifier) {
    hide = $(identifier).hide();
    show === "show" ? $(identifier).show() : hide;
}


//function to disable buttons
function disableEnableButton(identifier, disabled) {
    let disable = $(identifier).attr("disabled", true);
    (disabled === true) ? disable : $(identifier).attr("disabled", false)

}

//function to add commas
function numbersWithCommas(x) {
    x = x.toFixed(2);
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}



